var FCM = require('fcm-node');

var serverKey = 'AAAATtAv18k:APA91bFfkOHdxWSHMapAqQ8k-GIl8V32-cTEWLLT-4kV27OsJ_e_zXaP0uuHYWNc83gFxzOvuyLi8cbTbdUlsjkI-iVDzS0v4Y5ifBHxOlxAR9hBnQa_GyRSzwWHZO-ADV6WgVtMPskw';
var fcm = new FCM(serverKey);

var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
    to: 'eiYkKRgvOmI:APA91bFvBO-dhgnzL8OUg8HFdeHzxRScXa-27veM0aozVw6KbwgXK5O_ZJSScxoxbGeH5--JClU6bf-GxqNQTBXQqmT9xV1OSE04DOjL1VGLCB5Mgl-PTBa8tfLPo6lIoAzcYjO5feIG',
    collapse_key: '123',

    notification: {
        title: 'Title of your push notification',
        body: 'Body of your push notification'
    },

    data: {  //you can send only notification or only data(or include both)
        my_key: '123',
        my_another_key: 'my another value'
    }
};

fcm.send(message, function(err, response){
    if (err) {
        console.log("Something has gone wrong: "+err);
    } else {
        console.log("Successfully sent with response: ", response);
    }
});